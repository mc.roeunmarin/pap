const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

function startApp() {
    const today = new Date()
    let spanMoney = document.getElementById('span-money')
    let spanMinute = document.getElementById('span-minute')
    let button = document.getElementById('btn')
    let spanButton = document.getElementById('span-btn')
    let spanStart = document.getElementById('span-start')
    let spanStop = document.getElementById('span-stop')

    if (spanButton.textContent.includes('Start')) {
        spanButton.innerHTML = setIcons('fa fa-stop', 'Stop')
        spanStart.innerHTML = formatTime(today)
        button.className = 'p-1 btn btn-danger w-25 m-lg-1 m-md-1 mb-sm-1'
        
    } else if (spanButton.textContent.includes('Stop')) {
        if (spanStart.innerHTML !== '0:00') {
            spanButton.innerHTML = setIcons('fa fa-trash', 'Clear')
            button.className = 'p-1 btn btn-warning w-25 m-lg-1 m-md-1 mb-sm-1'
            spanStop.innerHTML = formatTime(today)
            let stopTime = document.getElementById('span-stop').textContent
            let startTime = document.getElementById('span-start').textContent
            let calculateMinutes = (convert24(stopTime)) - convert24(startTime)
            spanMinute.innerHTML = calculateMinutes.toString()
            spanMoney.innerHTML = calculateMoney(calculateMinutes)
        } else {
            spanButton.innerHTML = setIcons('fa fa-trash', 'Stop')
            button.className = 'p-1 btn btn-danger w-25 m-lg-1 m-md-1 mb-sm-1'
        }
    } else {
        spanButton.innerHTML = setIcons('fa fa-play', 'Start')
        button.className = 'p-1 btn btn-success w-25 m-lg-1 m-md-1 mb-sm-1'
        spanStart.innerHTML = '0:00'
        spanStop.innerHTML = '0:00'
        spanMinute.innerHTML = '0'
        spanMoney.innerHTML = '0'
    }
}

function setIcons(fontName, text) {
    let btnIcons = document.getElementById('btn-icon')
    removeElement(btnIcons)
    return `<i id='btn-icon' class='${fontName}'></i> <span id='span-btn'>${text}</span>`
}

function convert24(hour) {
    let hourWithoutAmPm = hour
        .toString()
        .replace(' AM', '')
        .replace(' PM', '')
    let separators = hourWithoutAmPm.split(":")
    let ampm = hour.toString().includes(" AM") ? 0 : 12 * 60
    return (Number(separators[0]) * 60) + (Number(separators[1])) + Number(ampm)
}

function calculateMoney(calculateMinutes) {
    if (calculateMinutes > 60) {
        return calculateMoney(calculateMinutes - 60) + 1500
    } else if (calculateMinutes <= 15) {
        return 500
    } else if (calculateMinutes <= 30) {
        return 1000
    } else if (calculateMinutes <= 60) {
        return 1500
    }
}


function currentTime() {
    const today = new Date()
    let year = today.getFullYear()
    let date = today.getDate()
    let month = today.getMonth()
    let day = today.getDay()
    document.getElementById("span-date-time").innerHTML
        = weekday[day]
        + " "
        + monthNames[month]
        + " "
        + date
        + " "
        + year
        + " "
        + formatAMPM(today)

    setTimeout(() => {
        currentTime()
    }, 1000)
}

function removeElement(element) {
    let elem = element
    return elem.parentNode.removeChild(elem)
}


function formatTime(date) {
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let ampm = hours >= 12 ? 'PM' : 'AM'
    hours = hours % 12
    hours = hours ? hours : 12
    minutes = minutes < 10 ? '0' + minutes : minutes
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime
}

function formatAMPM(date) {
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let second = date.getSeconds()
    let ampm = hours >= 12 ? 'PM' : 'AM'
    hours = hours % 12
    hours = hours ? hours : 12
    minutes = minutes < 10 ? '0' + minutes : minutes
    second = second < 10 ? '0' + second : second
    var strTime = hours + ':' + minutes + ':' + second + ' ' + ampm;
    return strTime;
}